import requests
import os
import shutil


def codewriter(address):
    a = 1
    for i in address:
        content = requests.get(i, stream=True)
        dump = content.raw
        location = os.path.abspath("/home/ratmir/HomeProject/page_codes")
        with open(str(a) + ".http", "wb") as location:
            shutil.copyfileobj(dump, location)
        a += 1


codewriter(["https://stackoverflow.com/questions/19602931/basic-http-file-downloading-and-saving-to-disk-in-python"])
